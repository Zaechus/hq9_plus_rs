use std::{
    convert::TryInto,
    env,
    fs::{self, File},
    io::{BufReader, Read},
};

use hq9_plus_rs::{run_once, run_shell};

fn main() {
    if let Some(path) = env::args().nth(1) {
        let mut code = if let Ok(metadata) = fs::metadata(&path) {
            String::with_capacity(metadata.len().try_into().unwrap())
        } else {
            String::new()
        };

        if let Ok(code_file) = File::open(path) {
            let mut file_reader = BufReader::new(code_file);
            file_reader
                .read_to_string(&mut code)
                .expect("Unable to read string from file");

            run_once(&code);
        } else {
            println!("The file could not be opened.");
        }
    } else {
        run_shell();
    }
}
